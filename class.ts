import { logmatrix, marsRover, moonRover} from "./index";

export class Rover{
    x:number;
    y:number;
    direction:string;
    log:[number,number];
    logarray:[number,number][];
    directionArray:string[];
    vector: {a:number,b:number}[];
    grid:number;

    constructor(x:number,y:number,direction:string){
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.log = [this.x,this.y];
        this.logarray = [this.log];
        this.directionArray = ["EAST","SOUTH","WEST", "NORTH"];
        this.vector = [{ a : 1 , b : 0 }, { a : 0 , b : -1 }, { a : -1 , b : 0 }, { a : 0 , b : 1 } ];
        this.grid = 10
        }

    moveForward(){
            const index = this.directionArray.indexOf(this.direction);
            this.x = this.x + this.vector[index].a;
            this.y = this.y + this.vector[index].b;
            this.log = [this.x,this.y];
            this.logarray.push(this.log);
            console.log (`Moving towards ${this.direction} to coordinates ${this.log}`);
        } 

    turnRight(){
        this.direction = this.directionArray[(this.directionArray.indexOf(this.direction) + 1)%4];
        console.log (`Turning right to face ${this.direction}.`);
    }    
        
    turnLeft(){
        this.directionArray = this.directionArray.reverse(); 
        this.direction = this.directionArray[(this.directionArray.indexOf(this.direction) + 1)%4];
            console.log (`Turning left to face ${this.direction}.`);
            this.directionArray = this.directionArray.reverse(); 

    }

    printPosition(){
        console.log (`Currently positioned in ${this.log}`)
    }

    printLog(rover:Rover){            
    for (let j = 0; j < this.logarray.length; j++){
    const i = logmatrix.length - 1 - this.logarray[j][1];
    if (rover == marsRover){
        logmatrix[i] = logmatrix[i].substring(0,this.logarray[j][0]) + "x" + logmatrix[i].substring(this.logarray[j][0] + 1);
    }else if (rover == moonRover){
        logmatrix[i] = logmatrix[i].substring(0,this.logarray[j][0]) + "y" + logmatrix[i].substring(this.logarray[j][0] + 1); 
    }
    }
    logmatrix.map((e) => console.log(e.split('').join(' ')));
    }

    
    
}