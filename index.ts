import * as readline from "readline-sync"
import { Rover } from "./class";
import { coordinateCheck, directionCheck } from "./functions";
import {controlRover} from "./commands";


        //class Rover definition

        

        // define a coordinate grid    
        export let logmatrix:string[]=[];
            for (let i = 0; i < 11; i++){
                let sym = '';
                for (let j = 0; j < 11;j++){
                    sym = sym + '-';
                }
                logmatrix.push(sym);
            }    
        // mapping an array of obstacles to the coordinate grid    
        let obstacleArray: [number,number][] = [[4,5],[2,3],[5,6]];
        for (let j = 0; j < obstacleArray.length; j++){
            const i = logmatrix.length - 1 - obstacleArray[j][1];
                logmatrix[i] = logmatrix[i].substring(0,obstacleArray[j][0]) + "0" + logmatrix[i].substring(obstacleArray[j][0] + 1);
            }
        // setting the initial positions and directions of the robots    
        console.log (`$Set the initial position of the Mars rover robot.
        $Enter the position coordinates (x,y) and the direction to face (east, west, north or south).`)
        let [x1,y1] = coordinateCheck(obstacleArray);
        let direction1 = directionCheck();
        export let marsRover = new Rover(x1,y1,direction1);

        console.log (`$Set the initial position of the Moon rover robot.
        $ßEnter the position coordinates (x,y) and the direction to face (east, west, north or south).`)
        let [x2,y2] = coordinateCheck(obstacleArray);
        let direction2 = directionCheck();
        export let moonRover = new Rover(x2,y2,direction2);
        
        // activate the user specified robot
        const activate = ():void => {
            console.log(`$Type R1 to activate Mars rover robot or type R2 to activate Moon rover robot: `)
            const robot:string = readline.question (`> `);
            if (robot == "R1"){
                console.log(`$Welcome! This is Mars rover robot. 
                $Control me by typing commands. Type help to see the list of available commands`)                
                return controlRover(marsRover);
            } else if (robot == "R2") {
                console.log(`$Welcome! This is Moon rover robot. 
                $Control me by typing commands. Type help to see the list of available commands`)
                return controlRover(moonRover);
            } console.log(`$Invalid command`);
            return activate();
        } 
        activate();




