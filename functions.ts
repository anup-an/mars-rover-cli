import * as readline from "readline-sync";

//checking x coordinate input
        

    export const coordinateCheck = (obstacleArray:[number,number][]):[number,number] => {
            const xCheck = ():number => {
                console.log(`$Enter the x-coordinate in the range 0 <= x < 10: `);
                const x:number|null = parseInt(readline.question("> "));
            
                if (x >= 0 && x <= 10){
                        return x;
                        } else {
                        console.log(`$Please enter a valid x-coordinate.`);
                        return xCheck();            
                    }
                }

                const yCheck = ():number => {
                    console.log(`$Enter the y-coordinate in the range 0 <= y < 10: `)
                
                    const y:number|null = parseInt(readline.question("> "));
                    if (y >= 0 && y <= 10){
                        return y;
                        } else {
                            console.log(`$Please enter a valid y-coordinate.`);
                            return yCheck();           
                        }
                    }
                    let x = xCheck();
                    let y = yCheck();

            const xIndex = obstacleArray.map(e => e[0]).indexOf(x);
            const yIndex = obstacleArray.map(e => e[1]).indexOf(y);
            if ( (xIndex == yIndex) && (xIndex != -1) && (yIndex != -1) ){
                console.log (`$There is an obstacle at ${x},${y}. Please place the robot in other positions in the grid.`);
                return coordinateCheck(obstacleArray);
            } return [x,y];
        }




    //checking direction input

        export const directionCheck = ():string => {
            const directionArray = ["EAST","WEST","NORTH", "SOUTH"];
            console.log("Enter the direction towards which the robot is facing: ");
            const direction:string|null = readline.question("> ").toUpperCase();
            const index = directionArray.find(e => e == direction);
            console.log(index);

            if (!index){
                console.log(`Please enter a valid direction ("east","west","north", or "south").`);
                return directionCheck();
                }else if (index){
                return index;                
            } throw new Error("Shouldn't be reachable");            
        }
