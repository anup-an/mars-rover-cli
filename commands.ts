import * as readline from "readline-sync";
import { Rover } from "./class";
import { marsRover, moonRover} from "./index";



export const helpCommand = () => {
    console.log(`Enter one of the following commands to control the robot: 
right    -- turns the robot to the right 
left     -- turns the robot to the right
forward  -- moves the robot forward by 1 unit
position -- current position of the robot
log      -- travel history of the robot
switch   -- switch control to another robot
`)
}
// checks the obstacles for the robot and moves it forward
export const obstacle = (rover:Rover):void => {
    const obstacleArray: [number,number][] = [[4,5],[2,3],[5,6]];
    const rovers = [marsRover,moonRover];
    // @ts-ignore: rover1 is never undefined
    const rover1:Rover = rovers.find(e => e != rover);

    if (rover){
        const xpos:number = rover.x;
        const ypos:number = rover.y;    
        rover.moveForward();
    const xIndex = obstacleArray.map(e => e[0]).indexOf(marsRover.x);
    const yIndex = obstacleArray.map(e => e[1]).indexOf(marsRover.y);

    if (rover.x > rover.grid || rover.y > rover.grid || rover.x < 0 || rover.y < 0 ){
        console.log (`$Manuevre not possible. The robot is at the edge of the grid.
        $Please turn left or right`);
        rover.x = xpos;
        rover.y = ypos;
        rover.log = [xpos,ypos];
        rover.logarray.pop();

    }else if ( ((xIndex == yIndex) && (xIndex != -1) && (yIndex != -1)) || ((rover.x == rover1.x) && (rover.y == rover1.y)) ){
        console.log (`$Collision warning!! There is an obstacle ahead at ${rover.x},${rover.y}. 
        $Please turn right or left to continue.`);
        rover.x = xpos;
        rover.y = ypos;
        rover.log = [xpos,ypos];
        rover.logarray.pop();

    }
    }
    
    
} 
//obstacle move forward


// moves the robot based on the inputs
export const controlRover = (rover:Rover):void => {
    const command:string = readline.question(`>  `);
    if (command == "right"){
        rover.turnRight();
        return controlRover(rover);
    }else if (command == "left"){
            rover.turnLeft();
            return  controlRover(rover);

    }else if (command == "forward"){
        obstacle(rover);
        return controlRover(rover);           
    } else if (command == "position"){
        rover.printPosition();
        return controlRover(rover); 
    }else if (command == "log"){
        rover.printLog(rover);
        return controlRover(rover); 
    }else if (command == "switch"){
            // @ts-ignore: roverSwitch is never undefined
        const roverSwitch:Rover = [marsRover,moonRover].find(e => e !=rover); 
        return controlRover(roverSwitch);
        }
        console.log(`Invalid command. Type help to see the list of available commands.`)        
        return controlRover(rover); 

        }
    

